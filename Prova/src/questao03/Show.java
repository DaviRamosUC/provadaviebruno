package questao03;

public class Show {
	
	private String name;

	public Show() {
	}

	public Show(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Nome do show:" + name;
	}
	
}
