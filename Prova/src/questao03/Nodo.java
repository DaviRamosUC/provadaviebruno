package questao03;

public class Nodo {
	
	private Show valor;
	private Nodo proximo;
	

	public Show getValor() {
		return valor;
	}

	public void setValor(Show valor) {
		this.valor = valor;
	}

	public Nodo getProximo() {
		return proximo;
	}

	public void setProximo(Nodo proximo) {
		this.proximo = proximo;
	}
	

}
