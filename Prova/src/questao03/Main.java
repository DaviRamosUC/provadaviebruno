package questao03;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
	
		Scanner sc = new Scanner(System.in);
		
		ListaEncadeada listaEncadeada = new ListaEncadeada();
		//Interface que leva ao m�todo de adicionar
		add(sc,listaEncadeada);
		//M�todo de ver tamanho
		System.out.println("Tamanho da lista: "+listaEncadeada.size());
		System.out.println(listaEncadeada.listar());
		//------------------------
		System.out.println("Informe o nome de um Show para excluir: ");
		String nome = sc.nextLine();
		//M�todo de remover
		listaEncadeada.remove(nome);
		System.out.println("Tamanho da lista: "+listaEncadeada.size());
		//Este � o m�todo para listar 
		System.out.println(listaEncadeada.listar());
		
		sc.close();

	}
	
	//Este m�todo � apenas para ter uma interface
	public static void add(Scanner sc, ListaEncadeada listaEncadeada) {
		boolean op;

		do {
			System.out.print("Informe o nome do Show: ");
			String nomeShow = sc.nextLine();			
			
			//Este � o m�todo que faz a adi��o do show
			listaEncadeada.add(new Show(nomeShow));
			
			System.out.print("Deseja cadastrar um outro Show? s/n: ");
			char resp = sc.nextLine().charAt(0);
			op = (resp == 's') ? true : false;
		} while (op);

	}

}
