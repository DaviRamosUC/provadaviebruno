package questao02;

import java.util.Scanner;

public class Questao02 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Informe um valor n: ");
		int n= sc.nextInt();
		
		System.out.println("O valor da somat�ria de 1 at� "+ n + " �: "+soma(n));
		sc.close();
	}
	// n = 4
	//1 + (1+1) + ((1+1)+1) + (((1+1)+1)+1)
	private static int soma(int n) {
		int soma = 1;
		if(n>1) {
			soma= n + soma(n-1);
		}else {
			soma = 1;
		}
		
		return soma;
		
	}

}
