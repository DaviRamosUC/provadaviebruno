package questao01;

import java.util.List;

public interface ConvidadoSet {
	
	void adicionar(List<Convidado> convidados, Convidado convidado);
}
