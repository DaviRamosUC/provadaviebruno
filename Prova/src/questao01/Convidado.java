package questao01;

import java.util.List;

public class Convidado implements ConvidadoSet{
	
	private String nome;
	private String whatsapp;
	
	public Convidado() {
	}

	public Convidado(String nome, String whatsapp) {
		this.nome = nome;
		this.whatsapp = whatsapp;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getWhatsapp() {
		return whatsapp;
	}

	public void setWhatsapp(String whatsapp) {
		this.whatsapp = whatsapp;
	}

	@Override
	public void adicionar(List<Convidado> convidados, Convidado convidado) {
		convidados.add(convidado);		
	}
	
	
}
