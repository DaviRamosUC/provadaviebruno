package questao04;

import java.util.Scanner;

public class Program {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		ListaEncadeada lista = new ListaEncadeada();
		//Interface que leva ao m�todo de adicionar
		add(sc,lista);

		//Este � o m�todo para listar
		System.out.println(lista.listar());
		
		System.out.println("Retirando do fim... Aguarde");
		//M�todo de remover
		lista.removeFim();
		
		System.out.println("Retirado:");
		System.out.println(lista.listar());

	}
	
	//Este m�todo � apenas para ter uma interface
		public static void add(Scanner sc, ListaEncadeada listaEncadeada) {
			boolean op;

			do {
				System.out.print("Informe o nome do Passageiro: ");
				String nomePassageiro = sc.nextLine();			
				
				//Este � o m�todo que faz a adi��o do passageiro
				listaEncadeada.addFim(new Passageiro(nomePassageiro));
				
				System.out.print("Deseja cadastrar um outro Passageiro? s/n: ");
				char resp = sc.nextLine().charAt(0);
				op = (resp == 's') ? true : false;
			} while (op);

		}

}
