package questao04;

public class Nodo {
	
	private Passageiro valor;
	private Nodo proximo;
	private Nodo anterior;
	
	public Nodo(Passageiro valor) {
		this.valor = valor;
	}

	public Nodo(Passageiro valor, Nodo proximo) {
		this.valor = valor;
		this.proximo = proximo;
	}

	public Passageiro getValor() {
		return valor;
	}

	public void setValor(Passageiro valor) {
		this.valor = valor;
	}

	public Nodo getProximo() {
		return proximo;
	}

	public void setProximo(Nodo proximo) {
		this.proximo = proximo;
	}

	public Nodo getAnterior() {
		return anterior;
	}

	public void setAnterior(Nodo anterior) {
		this.anterior = anterior;
	}
	
	

}
