package questao04;

public class ListaEncadeada {

	private Nodo primeiro;
	private Nodo ultimo;
	private int size = 0;

	public void addFim(Passageiro valor) {
		if(size == 0) {
			Nodo novoNodo = new Nodo(valor);
			primeiro = novoNodo;
			ultimo = novoNodo;
		}else {
			Nodo novoNodo = new Nodo(valor);
			ultimo.setProximo(novoNodo);
			novoNodo.setAnterior(ultimo);
			ultimo=novoNodo;
		}
		size++;
	}
	
	public void removeFim() {
		if (size == 0) {
			throw new RuntimeException("A lista est� vazia");
		}else {
			Nodo penultimo = ultimo.getAnterior();
			penultimo.setProximo(null);
			ultimo = penultimo;
			size--;
		}
	}
	
	public String listar() {
		Nodo ponteiro = primeiro;
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < size; i++) {
			sb.append(ponteiro.getValor());
			sb.append("\n");
			ponteiro= ponteiro.getProximo();
		}
		return sb.toString();
	}

	public Nodo getPrimeiro() {
		return primeiro;
	}

	public void setPrimeiro(Nodo primeiro) {
		this.primeiro = primeiro;
	}

	public Nodo getUltimo() {
		return ultimo;
	}

	public void setUltimo(Nodo ultimo) {
		this.ultimo = ultimo;
	}	
	
}
